public class sample implements add {

	public class A1 {
		A1() {
			System.out.println("A1 instance!");
		}

		void doIt(sample sam) {
			sample2 object = new sample2();
			try {
				object.add(sam, 1, 2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		sample sam = new sample();
		A1 a = sam.new A1();
		a.doIt(sam);
	}

	@Override
	public void back() {
		System.out.println("Done!");
	}

}
